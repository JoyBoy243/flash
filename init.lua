minetest.register_node("flash:flashbloc", {
    description = "Ceci est un bloc pour aller vite",
    tiles = {"face1.png"},
    -- pour qu'on puisse casser facilement le bloc:
    groups = { oddly_breakable_by_hand = 1},
    -- pour définir ce qui se passe quand on casse le bloc:
    on_punch = function(pos, node, puncher, pointed_thing)
        local playerspeed = puncher:get_physics_override().speed
        if playerspeed > 1 then
            puncher:set_physics_override({
                speed = 1,
            })
        elseif playerspeed == 1 then
            puncher:set_physics_override({
                speed = 5,
            })
        end
    end, 
})